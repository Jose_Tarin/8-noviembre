<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
?>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

        <title><?=$title?></title>
    </head>
    <body >
        <div style="background-color:lightyellow; border: 3px solid grey">
        <br>
    <center><h1 class="text-secondary"><b><?= $title?><b></h1></center>
      <br>
      </div>
        <table class="table table-striped" style="border: 3px grey solid">
               <tr>
                    <td style="border: 2px solid black">
                        <p>CodigoProductos</p>
                    </td>
                   <td style="border: 2px solid black">
                        <p>Nombre</p>
                    </td>
                    <td style="border: 2px solid black">
                        <p>CodigoFamilia</p>
                    </td>
                    <td style="border: 2px solid black">
                        <p>Caracteristicas</p>
                    </td>
                    <td style="border: 2px solid black">
                        <p>Color</p>
                    </td>
                    <td style="border: 2px solid black">
                        <p>TipoIva</p>
                    </td>
                     <td style="border: 2px solid black">
                        <p>Opciones</p>
                    </td>
                </tr>
            <?php foreach ($resultado as $productos): ?>
                <tr>
                    <td style="border: 2px solid black">
                        <?= $productos->CodigoProducto ?>
                    </td>
                    <td style="border: 2px solid black">
                        <?= $productos->Nombre ?>
                    </td>
                    <td style="border: 2px solid black">
                        <?= $productos->CodigoFamilia ?>
                    </td>
                      <td style="border: 2px solid black">
                        <?= $productos->Caracteristicas ?>
                    </td>
                      <td style="border: 2px solid black">
                        <?= $productos->Color ?>
                    </td>
                      <td style="border: 2px solid black">
                        <?= $productos->TipoIVA ?>
                    </td>
                    <td style="border: 2px solid black">
                        <form action ="<?=site_url('productos/delete')?>" method="post">
                            <input class="btn btn-danger" type="submit" name="Borrar" value="Borrar"/>
                        </form>                    </td>
                </tr>

            <?php endforeach; ?>
        </table>
    </body>
</html>

