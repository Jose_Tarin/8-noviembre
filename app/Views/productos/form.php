<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
?>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

        <title><?= $title?></title>
    </head>
    <body>
        <br><br><br><!-- comment -->
        <div class="container">
            <h1 class="text-primary"><b><?= $title ?></b></h1>
            <form action="<?= site_url('productos/insert')?>" method="post">
                <div class="form-group">
                    <label for="CodigoProductos">Codigo del Producto</label>
                    <input type="text" name="CodigoProducto" value="" id="CodigoProducto" class="form-control"/>
                </div>
                <div class="form-group">
                    <label for="Nombre">Nombre del Producto</label>
                    <input type="text" name="Nombre" value="" id="Nombre" class="form-control"/>
                </div><!-- comment -->
                <div class="form-group">
                    
                    <label for="CodigoFamilia">Familia del Productos</label>
                    <select name="CodigoFamilia" id="CodigoFamilia">
        <option value="1000">ORTÉSICA</option>
        <option value="2000">CALZADO</option>
        <option value="3000">MASTECTOMÍA</option>
        <option value="4000">AYUDAS TÉCNICAS</option>
        <option value="5000">PROTÉSICA</option>
        <option value="6000">SUMINISTROS</option>
        <option value="7000">CENTRAL DE FABRICACIÓN</option>
      </select>
                    <!--<input type="text" name="CodigoFamilia" value="" id="CodigoFamilia" class="form-control"/>-->
                    
                </div><!-- comment -->
                <div class="form-group">
                    <label for="Caracteristicas">Caracteristicas del Productos</label>
                    <input type="text" name="Caracteristicas" value="" id="Caracteristicas" class="form-control"/>
                </div><!-- comment --><div class="form-group">
                    <label for="Color">Color del Productos</label>
                    <input type="text" name="Color" value="" id="Color" class="form-control"/>
                </div><!-- comment --><div class="form-group">
                    <label for="TipoIva">Tipo de Iva</label>
                    <input type="text" name="TipoIva" value="" id="TipoIva" class="form-control"/>
                </div>
                <input type="submit" name="enviar" value="Enviar"/>
            
                
                
                
                
                
            </form>
        </div>
        
        