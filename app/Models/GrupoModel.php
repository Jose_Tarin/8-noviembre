<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Models;
use CodeIgniter\Model;
/**
 * Description of GrupoModel
 *
 * @author a023886021s
 */
class GrupoModel extends Model {
    protected $table = 'grupos';
    protected $primaryKey = 'id';
    protected $returnType = 'object';
    protected $allowedFields = ['codigo','nombre'];
}
