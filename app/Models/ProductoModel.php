<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Models;
use CodeIgniter\Model;

/**
 * Description of ProductoModel
 *
 * @author Jose Tarin
 */
class ProductoModel extends Model {
    protected $table = 'productos';
    protected $primaryKey = 'CodigoProducto';
    protected $returnType = 'object';
    protected $allowedFields = ['CodigoProducto','Nombre','CodigoFamilia','Caracteristicas','Color','TipoIVA'];
}
