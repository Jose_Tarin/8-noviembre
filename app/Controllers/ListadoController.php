<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;
use App\Models\ProductoModel;

/**
 * Description of ListadoController
 *
 * @author Jose Tarin
 */
class ListadoController extends BaseController{
   
 
   public function mostrarLista() {
        $productos = new \App\Models\ProductoModel();
        $data['resultado'] = $productos->findAll();
        $data['title'] = 'Listado de Productos';
        return view('productos/list', $data);
  
       
   }
   public function deleteProducto(){
        //Despues de hacer el delete la sentencia de abajo volvera a cargar
        //la pagina y por tanto se refrescará con los datos eliminados.
        $CodigoProductos = $this -> request -> getPost('CodigoProductos');
         $Nombre = $this -> request -> getPost('Nombre');
          $CodigoFamilia = $this -> request -> getPost('CodigoFamilia');
           $Caracteristicas = $this -> request -> getPost('Caracteristicas');
            $Color = $this -> request -> getPost('Color');
             $TipoIva= $this -> request -> getPost('TipoIva');
        
        $producto_deleted = [
            'CodigoProductos'=>  $CodigoProductos,
            'Nombre'=>  $Nombre,
            'CodigoFamilia'=>  $CodigoFamilia,
            'Caracteristicas'=>  $Caracteristicas,
            'Color'=>  $Color,
            'TipoIva'=>  $TipoIva,          
        ];
       $productoModel = new ProductoModel();
       $productoModel -> delete($producto_deleted);
       return redirect()->to('productos/list');
   }
   
   
   
   
   
   
   
   public function mostrarForm() {
         $data['title'] = 'Nuevos Productos';
        return view('productos/form', $data);
  
       
   }
    public function insertProducto(){
        //Despues de hacer el delete la sentencia de abajo volvera a cargar
        //la pagina y por tanto se refrescará con los datos eliminados.
        $CodigoProducto = $this -> request -> getPost('CodigoProducto');
         $Nombre = $this -> request -> getPost('Nombre');
          $CodigoFamilia = $this -> request -> getPost('CodigoFamilia');
           $Caracteristicas = $this -> request -> getPost('Caracteristicas');
            $Color = $this -> request -> getPost('Color');
             $TipoIva= $this -> request -> getPost('TipoIva');
        
        $producto_nuevo = [
            'CodigoProducto'=>  $CodigoProducto,
            'Nombre'=>  $Nombre,
            'CodigoFamilia'=>  $CodigoFamilia,
            'Caracteristicas'=>  $Caracteristicas,
            'Color'=>  $Color,
            'TipoIva'=>  $TipoIva,          
        ];
        
 
       $productoModel = new ProductoModel();
       $productoModel -> insert($producto_nuevo);
       return redirect()->to('productos/form');
   }
    
    
}
