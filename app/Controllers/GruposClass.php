<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace App\Controllers;
use App\Models\GrupoModel;
/**
 * Description of GruposClass
 *
 * @author a023886021s
 */
class GruposClass extends BaseController{
    public function grupos() {
        $grupoModel = new GrupoModel();
      /* echo "<pre>";
            print_r($grupoModel-> findAll());
        echo "</pre>";*/
        $data['titulo'] = 'Listado de Grupos';
        $data['grupos'] = $grupoModel-> findAll();
        return view('grupos/list',$data);
    }
    
   public function gruposEdit($id){
        helper('form');
        $grupoModel = new GrupoModel();
        $data['titulo'] = 'Editar Grupos';
        $data['grupos'] = $grupoModel->find($id);
        /*echo '<pre>';
        print_r($data);
        echo '</pre>';*/
        return view('grupos/edit',$data);
    }
    public function actualiza($id){
        //recoger datos
        $grupo = $this->request->getPost();
        unset($grupo['botoncito']);
      echo '<pre>';
        echo $id;
        print_r($grupo);
        echo '</pre>';
        $grupoModel = new GrupoModel();
        $grupoModel->update($id,$grupo);
        return redirect()->to('grupos');
    }


}
